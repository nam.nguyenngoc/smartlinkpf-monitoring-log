import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'User Profile',
          path: 'pages/user',
          component: () => import('@/views/dashboard/pages/UserProfile'),
        },
        {
          name: 'Notifications',
          path: 'components/notifications',
          component: () => import('@/views/dashboard/component/Notifications'),
        },
        {
          name: 'Icons',
          path: 'components/icons',
          component: () => import('@/views/dashboard/component/Icons'),
        },
        {
          name: 'Typography',
          path: 'components/typography',
          component: () => import('@/views/dashboard/component/Typography'),
        },
        // Tables
        {
          name: 'Regular Tables',
          path: 'tables/regular-tables',
          // component: () => import('@/views/dashboard/tables/RegularTables'),
          // component: () => import('@/views/dashboard/sales/SaleStorage'),
        },
        // Maps
        {
          name: 'Google Maps',
          path: 'maps/google-maps',
          component: () => import('@/views/dashboard/tables/RegularTables'),
        },
        // Upgrade
        {
          name: 'Upgrade',
          path: 'upgrade',
          component: () => import('@/views/dashboard/Upgrade'),
        },
        // Upgrade
        // {
        //   name: 'QLSANPHAM',
        //   path: '/sales/salestorage',
        //   component: () => import('@/views/sales/SaleStorage'),
        // },
        // // Tra gop
        // {
        //   name: 'Quản lý trả góp',
        //   path: '/kinhte/TraGop',
        //   component: () => import('@/views/kinhte/TraGop'),
        // },
        // // Bán hàng
        // {
        //   name: 'Bán hàng',
        //   path: '/sales/saleorder',
        //   component: () => import('@/views/sales/SaleOrder'),
        // },
        // // Thue nha
        // {
        //   name: 'THU CHI THUÊ NHÀ',
        //   path: '/kinhte/ThueNha',
        //   component: () => import('@/views/kinhte/ThueNha'),
        // },
        // // List
        // {
        //   name: 'DANH SÁCH ĐƠN HÀNG',
        //   path: '/sales/saleorderlist',
        //   component: () => import('@/views/sales/SaleOrderList'),
        // },
        // // Them san pham
        // {
        // name: 'THÊM SẢN PHẨM',
        // path: '/sales/AddProduct',
        // component: () => import('@/views/sales/AddProduct'),
        // },
        // {
        //   name: 'CHI TIET SAN PHAM',
        //   path: '/sales/detailProduct/:id',
        //   component: () => import('@/views/sales/detailProduct'),
        // },
        // {
        //   name: 'CHI TIÊU GIA ĐÌNH',
        //   path: '/kinhte/chitieus',
        //   component: () => import('@/views/kinhte/ChiTieus'),
        // },
        // {
        //   name: 'CHI TIẾT CHI TIÊU',
        //   path: '/kinhte/detailChiTieu',
        //   component: () => import('@/views/kinhte/detailChiTieu'),
        // },
        // {
        //   name: 'Thêm chi tiêu',
        //   path: '/kinhte/chitieuPageAdd',
        //   component: () => import('@/views/kinhte/chitieuPageAdd'),
        // },
        // {
        //   name: 'Chi tiet chi tieu',
        //   path: '/kinhte/chitieuPageDetail',
        //   component: () => import('@/views/kinhte/chitieuPageDetail'),
        // },
        {
          name: 'Login',
          path: '/login',
          component: () => import('@/views/Login'),
        },
        //Company
        //Workflow
        {
          name: 'Kafka Log Workflow',
          path: '/smartlink/kafka/workflow',
          component: () => import('@/views/smartlink/kafka/workflowkafka'),
        },
        {
          name: 'Jira Logtime',
          path: '/smartlink/jira/jiraLogtime',
          component: () => import('@/views/smartlink/jira/jiraLogtime'),
        },
        {
          name: 'Jira Ticket',
          path: '/smartlink/jira/jiraTickets',
          component: () => import('@/views/smartlink/jira/jiraTickets'),
        },
        {
          name: 'Logout',
          path: '/Logout',
          component: () => import('@/views/Logout'),
        },
        {
          name: 'Manager Team',
          path: '/smartlink/managerteam/manager',
          component: () => import('@/views/smartlink/managerteam/manager'),
        },
        {
          name: 'Weekly Report',
          path: '/smartlink/managerteam/weeklyRerport',
          component: () => import('@/views/smartlink/managerteam/weeklyRerport'),
        },
        {
          name: 'Blueprint Effort',
          path: '/smartlink/managerteam/blueprintEffort',
          component: () => import('@/views/smartlink/managerteam/blueprintEffort'),
        },
      ],
    },
  ],
})
