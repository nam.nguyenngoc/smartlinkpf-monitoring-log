// =========================================================
// * Nam Anh Family - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import VImageInput from 'vuetify-image-input'
import VuetifyMoney from "vuetify-money"
import VueApexCharts from 'vue-apexcharts'
import VueNumericInput from 'vue-numeric-input';
import vueCookies from 'vue-cookies';
import JsonViewer from 'vue-json-viewer'
// import VueLoading from 'vue-loading-overlay';
// import 'vue-loading-overlay/dist/vue-loading.css';

Vue.component(VImageInput.name, VImageInput)
Vue.use(VuetifyMoney)
Vue.use(VueApexCharts)
Vue.use(VueNumericInput)
Vue.component('apexchart', VueApexCharts)
Vue.use(vueCookies)
Vue.use(JsonViewer)
// Vue.use(VueLoading);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  i18n,
  // VueLoading,
  render: h => h(App),
}).$mount('#app')
