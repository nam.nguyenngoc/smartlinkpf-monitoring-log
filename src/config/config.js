/* eslint-disable no-unused-vars */

 const API_URL = 'http://anvatchibeo.ddns.net'; //  platform server
 
 const API_LC = 'http://127.0.0.1'; //  platform server
//  const API_KAFKA_URL = 'http://172.20.123.15'; //  platform server
const API_KAFKA_URL = 'http://172.20.123.15'; //  platform server
const API_KAFKA_URL_THANH = 'http://172.20.123.216'; //  platform server

const API_KAFKA_URL_DUNG = 'http://172.20.123.203'; //  platform server

const API_KAFKA_URL_KHANH = 'http://172.20.123.41'; //  platform server
// const API_URL = 'http://127.0.0.1' //  platform server
const PIM_URL = 'https://pim.cyberlogitec.com/jira/browse';

const sheetDashboardConfig = {
  sheetId: "145lekPGH3wyvf_6SBVGso6X2z2k2aI570CQXhwmVSRw", //SmartlinkPlatform Develop_Golive
  infos: {
    name: ["Bug & Improvement"],
    offset: 1,
    row_limit: 500
  }
}
const userValid = [
  'doudevnn',
  'nam.nguyenngoc'
]
// Only developing application connect to local ip ,another applications connect to developement server ip.
const config = {
  API_FAMILY: `${API_URL}:80/api`,
  // API_FAMILY: `${API_LC}:8789`,
  API_KAFKA_URL: `${API_KAFKA_URL}:8888`,
  API_DOWNLOAD: `${API_KAFKA_URL}:8080`,
  API_KAFKA_WORKFLOW: `${API_KAFKA_URL}:8888/comm/readFile128/N/SETUP`,
  API_KAFKA_URL_THANH: `${API_KAFKA_URL_THANH}:8888`,
  API_KAFKA_WORKFLOW_THANH: `${API_KAFKA_URL_THANH}:8888/comm/readFile128/N/SETUP`,
  API_KAFKA_URL_DUNG: `${API_KAFKA_URL_DUNG}:8888`,
  API_KAFKA_URL_KHANH: `${API_KAFKA_URL_KHANH}:8888`,
  API_KAFKA_WORKFLOW_DUNG: `${API_KAFKA_URL_DUNG}:8888/comm/readFile128/N/SETUP`,
  // API_FAMILY: `${API_URL}:8789`,
  dateTimeFormat: 'YYYYMMDD', // Format date time constant for condition search
  tooltipWidth: 300,
  API_JIRA: `${API_URL}:8888/api`,
  // API_JIRA: `${API_LC}:9789`,
  // API_JIRA: "http://192.168.1.254:9789",
  API_JIRA_LC: `${API_URL}:9789`,
  PIM_URL: PIM_URL,
  sheetDashboardConfig: `${sheetDashboardConfig}`,
  userValid: `${userValid}`,
  holidays:[
    { holidayDate: "2021-09-02", description: "Quoc Khanh" },
    { holidayDate: "2021-09-03", description: "Quoc Khanh" },
  ],
}

export default config
