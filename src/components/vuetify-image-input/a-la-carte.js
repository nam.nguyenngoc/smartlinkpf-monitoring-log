import VImageInput from './index';

import {
	VBtn,
	VIcon,
	VSlider,
	VSpacer,
} from 'vuetify/lib';

export default {
	components: {
		VBtn,
		VIcon,
		VSlider,
		VSpacer,
	},
	extends: VImageInput,
};
