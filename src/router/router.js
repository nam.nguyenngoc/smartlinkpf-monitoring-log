import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'User Profile',
          path: 'pages/user',
          component: () => import('@/views/dashboard/pages/UserProfile'),
        },
        {
          name: 'Notifications',
          path: 'components/notifications',
          component: () => import('@/views/dashboard/component/Notifications'),
        },
        {
          name: 'Icons',
          path: 'components/icons',
          component: () => import('@/views/dashboard/component/Icons'),
        },
        {
          name: 'Typography',
          path: 'components/typography',
          component: () => import('@/views/dashboard/component/Typography'),
        },
        // Tables
        {
          name: 'family-dashboards',
          path: 'tables/regular-tables',
          component: () => import('@/views/dashboard/tables/RegularTables'),
        },
        // Maps
        {
          name: 'Google Maps',
          path: 'maps/google-maps',
          component: () => import('@/views/dashboard/maps/GoogleMaps'),
        },
        // Upgrade
        {
          name: 'Upgrade',
          path: 'upgrade',
          component: () => import('@/views/dashboard/Upgrade'),
        }, // Upgrade
        // {
        //   name: 'QLSANPHAM',
        //   path: '/sales/salestorage',
        //   component: () => import('@/views/dashboard/Upgrade'),
        // }, {
        //   name: 'BÁN HÀNG',
        //   path: '/sales/saleorder',
        //   component: () => import('@/views/sales/SaleOrder'),
        // },
        // // Tra gop
        // {
        //   name: 'Quản lý trả góp',
        //   path: '/sales/TraGop',
        //   component: () => import('@/views/kinhte/TraGop'),
        // },
        // // Thue nha
        // {
        //   name: 'THU CHI THUÊ NHÀ',
        //   path: '/kinhte/ThueNha',
        //   component: () => import('@/views/kinhte/ThueNha'),
        // },
        // // Them san pham
        // {
        //   name: 'THÊM SẢN PHẨM',
        //   path: '/sales/AddProduct',
        //   component: () => import('@/views/sales/AddProduct'),
        // },
        // // THÊM CHI TIÊU
        // {
        //   name: 'Thêm chi tiêu',
        //   path: '/kinhte/chitieuPageAdd',
        //   component: () => import('@/views/kinhte/chitieuPageAdd'),
        // },
        // {
        //   name: 'Chi tiet chi tieu',
        //   path: '/kinhte/chitieuPageDetail',
        //   component: () => import('@/views/kinhte/chitieuPageDetail'),
        // },
        // {
        //   name: 'GoogleSheet_KinhDoanhDoAnVat',
        //   path: 'https://docs.google.com/spreadsheets/d/1-axypM8e0DyjNAJCFySpvmxUJNleR4grwy3EjlkRM3A/edit#gid=1410155223/kinhte/chitieuPageDetail',
        //   // component: () => import('@/views/kinhte/chitieuPageDetail'),
        // },
        {
          name: 'Login',
          path: '/login',
          component: () => import('@/views/login'),
        },
        {
          name: 'Logout',
          path: '/Logout',
          component: () => import('@/views/Logout'),
        },
        //Company
        //Workflow
        {
          name: 'Kafka Log Workflow',
          path: '/smartlink/kafka/workflow',
          component: () => import('@/views/smartlink/kafka/workflowkafka'),
        },
        // managerteam
        {
          name: 'Manager Team',
          path: '/smartlink/managerteam/manager',
          component: () => import('@/views/smartlink/managerteam/manager'),
        },
      ],
    },
  ],
})
