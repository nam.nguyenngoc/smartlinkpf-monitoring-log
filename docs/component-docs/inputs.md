# Inputs

To use the form group inputs please see below examples:

::: demo
```html
<v-text-field
  class="purple-input"
  label="Normal Input"
/>
<v-text-field
  class="purple-input"
  label="Disabled"
  disabled
/>
```
:::

> Note: For more details about the all inputs that you can use please see the **Inputs & Controls** section from [vuetifyjs.com](https://vuetifyjs.com/en/components/inputs)
