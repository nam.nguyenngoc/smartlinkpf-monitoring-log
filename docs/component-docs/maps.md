# Maps

For maps we've used the library an iframe who have an `src` attribute. You can use it by simply following the below example:

::: demo
```html
<div class="mapouter">
  <div class="gmap_canvas">
    <iframe
      id="gmap_canvas"
      width="100%"
      height="100%"
      src="https://maps.google.com/maps?q=google&t=&z=13&ie=UTF8&iwloc=&output=embed"
      frameborder="0"
      scrolling="no"
      marginheight="0"
      marginwidth="0"
    />
  </div>
</div>
```
:::
