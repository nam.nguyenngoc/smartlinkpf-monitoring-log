# Textarea

To use the custom form group inputs you don't need to import the custom made component, you can use it like this:

::: demo
```html
<v-textarea
  class="purple-input"
  label="About Me"
  value="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
/>
```
:::

> Note: For more details about the textarea please see the **Textarea** section from [vuetifyjs.com](https://vuetifyjs.com/en/components/textarea#introduction)
