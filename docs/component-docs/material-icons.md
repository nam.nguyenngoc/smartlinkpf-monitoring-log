# Material Icons

Through most of the examples in this dashboard, we have used the default Icons for the Material Design provided by Google. You can easily use them like this:

```html
<v-icon>
  icon
 </v-icon>
```

::: demo
```html
<v-icon>mdi-heart</v-icon>
```
:::

## Colors

The icons can be used in more colors:

::: demo
```html
<v-icon color="green">mdi-heart</v-icon>
<v-icon color="red">mdi-heart</v-icon>
<v-icon color="orange">mdi-heart</v-icon>
<v-icon color="blue">mdi-heart</v-icon>
<v-icon color="purple">mdi-heart</v-icon>
```
:::


> Note: for more details about the icons please see the **Icons** section from [vuetifyjs.com](https://vuetifyjs.com/en/framework/icons#introduction)
