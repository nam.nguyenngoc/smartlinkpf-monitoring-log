module.exports = {
  publicPath: "/",
  devServer: {
    disableHostCheck: true,
    https: false,
  },

  transpileDependencies: ['vuetify'],

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
  lintOnSave: false,
  chainWebpack: config => {
    config.resolve
      .alias
      .set('numeric-keyboard$', 'numeric-keyboard/dist/numeric_keyboard.vue.js')
    // fix error above by excluding node_modules
    config.module
      .rule('js')
        .exclude
        .add(/node_modules/)
        .end()
  }
}
