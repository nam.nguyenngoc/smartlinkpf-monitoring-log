"use strict";
// var pdfGenerator = require('template-pdf-generator');
var wkhtmltopdf = require('wkhtmltopdf');
var fs = require('fs');
const {makeid, makeIdByDateTime , utn} = require('../services/commonServices');
const https = require('https');
const http = require('http');

// // URL
// wkhtmltopdf('http://google.com/', { pageSize: 'A5' })
//   .pipe(fs.createWriteStream('out.pdf'));
  
// // HTML
// wkhtmltopdf('<h1>Test</h1><p>Hello world</p>')
//   .pipe(res);

// // Stream input and output
// var stream = wkhtmltopdf(fs.createReadStream('file.html'));

// // output to a file directly
// wkhtmltopdf('http://apple.com/', { output: 'out.pdf' });

// // Optional callback
// wkhtmltopdf('http://google.com/', { pageSize: 'letter' }, function (err, stream) {
//   // do whatever with the stream
// });

// // Repeatable options
// wkhtmltopdf('http://google.com/', {
//   allow : ['path1', 'path2'],
//   customHeader : [
//     ['name1', 'value1'],
//     ['name2', 'value2']
//   ]
// });

// // Ignore warning strings
// wkhtmltopdf('http://apple.com/', { 
//   output: 'out.pdf',
//   ignore: ['QFont::setPixelSize: Pixel size <= 0 (0)']
// });
// // RegExp also acceptable
// wkhtmltopdf('http://apple.com/', { 
//   output: 'out.pdf',
//   ignore: [/QFont::setPixelSize/]
// });


/*
var data = {
  name: 'World',
  name2: 'World2',
  name3: 'World3',
};

// var template = '<h1>Hello {{name}}</h1>';

var css = 'h1 {color: red}';
function pdfOrder(req, res, next) {
  var template = fs.readFileSync(`${process.env.PROJECT_DIR}/templates/orders/orderPrint.html`, 'utf8', function (err, data) {
    if (err) throw err;

    if (data) return data

  });
  pdfGenerator(data, template, css).pipe(fs.createWriteStream('out.pdf'));
}
const pdfPrinterServices = {
  pdfOrder: pdfOrder,
}
*/

function print (fileID) {
  http.get(`http://192.168.1.254:8080/print?url=http://anvatchibeo.ddns.net/${fileID}`, (resp) => {
    let data = '';
    // A chunk of data has been received.
    resp.on('data', (chunk) => {
      data += chunk;
    });
    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      console.log(JSON.parse(data).explanation);
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
}

function pdfOrder(req, res, next) {
  let status = "SUCCESS";
  let error = "";
  try {
    var template = fs.readFileSync(`${process.env.PROJECT_DIR}/templates/orders/orderPrint.html`, 'utf8', function (err, data) {
      if (err) throw err;
  
      if (data) return data
  
    });
    let orderId = makeIdByDateTime(utn('ORDER')).toUpperCase();
    wkhtmltopdf(template, { pageSize: 'A6' })
    .pipe(fs.createWriteStream(`${process.env.PROJECT_DIR}/sales/orders/${orderId}.pdf`));
  
  } catch (error) {
    status = "FAIL";
    error = error.message;
  }
  
  res.status(200)
  .json({
    status: status,
    data: "1",
    message: error
  });
  
}
const pdfPrinterServices = {
  pdfOrder: pdfOrder,
  print: print,
}

module.exports = pdfPrinterServices;
