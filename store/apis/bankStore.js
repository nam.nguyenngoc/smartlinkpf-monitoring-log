import axios from 'axios';
// import config from '@/vue-caas/config/config.js';

const baseURI = "http://localhost:3000";//config.setupApiUrl;

const bankStoreModule = {
  // namespaced: false,

  state: {
    bankList: [],
    // communicationSelectBoxList: [],
    // validCommunicationList: [],
    // communication: {},
    // communicationId: '',
    // dataSet01Source: [],
    // filter: {},
  },
  mutations: {
    'SET_STORE_BANK_LIST' (state, bankList) {
      state.bankList = bankList;
    //   state.dataSet01Source = bankList;
    },
    // 'SET_STORE_COMMUNICATION_SELECT_BOX_LIST' (state, communicationSelectBoxList) {
    //   state.communicationSelectBoxList = [];
    //   state.communicationList = communicationSelectBoxList;
    //   for (let i = 0; i < communicationSelectBoxList.length; i += 1) {
    //     const tmpComm = communicationSelectBoxList[i];
    //     const tmp = { text: `${tmpComm.name} (${tmpComm.type})`, value: tmpComm.id, }
    //     state.communicationSelectBoxList.push(tmp);
    //   }
    // },
    // 'SET_STORE_COMMUNICATION' (state, communication) {
    //   state.communication = communication;
    // },
    // 'SET_STORE_COMMUNICATION_ID' (state, id) {
    //   state.communicationId = id;
    // },
    // 'SET_STORE_VALID_COMMUNICATION_LIST' (state, communicationList) {
    //   state.validCommunicationList = communicationList;
    // },
    // 'SET_STORE_COMMUNICATION_FILTER'(state, filter) {
    //   let datarows = state.dataSet01Source;
    //   state.filter = filter;
    //   let filterBy = state.filter;
    //   let listResult = [];
    //   if (filterBy != null) {
    //     let filter = filterBy.filter;
    //     let str = filterBy.str;
    //     if (str.trim() == '') {
    //       listResult = datarows;
    //     }
    //     listResult = datarows.filter(item =>  {
    //       if(filter == 'all') {
    //         for (const prop in item) {
    //           if (typeof item[prop] == 'string') {
    //             let propValue = item[prop].toLowerCase();
    //             let queryValue = str.toLowerCase();
    //             if (propValue.includes(queryValue)) {
    //               return true;
    //             }
    //           } else if (typeof item[prop] == 'number') {
    //             let propValue = item[prop].toString().toLowerCase();
    //             let queryValue = str.toLowerCase();
    //             if (propValue.includes(queryValue)) {
    //               return true;
    //             }
    //           }
    //         }
    //       } else {
    //         if (typeof item[filter] == 'string') {
    //           let propValue = item[filter].toLowerCase();
    //           let queryValue = str.toLowerCase();
    //           if (propValue.includes(queryValue)) {
    //             return true;
    //           }
    //         } else if (typeof item[filter] == 'number') {
    //           let propValue = item[filter].toString().toLowerCase();
    //           let queryValue = str.toLowerCase();
    //           if (propValue.includes(queryValue)) {
    //             return true;
    //           }
    //         }
    //       }

    //       return false;
    //     });
    //   }
    //   state.communicationList = listResult;
    //   // state.filter = filter;
    // },
  },
  actions: {
    getBankList: async ({commit}, params) => {
      await axios.post(baseURI + "/apis/banks", params).then((response) => {
        commit('SET_STORE_BANK_LIST', response.data);
      }).catch((ex) => {
        // alert(ex);
        throw ex;
      });
    },

  },
  getters: {
    bankList: state => state.bankList,
    // communicationSelectBoxList: state => state.communicationSelectBoxList,
    // communication: state => state.communication,
    // communicationId: state => state.communicationId,
    // validCommunicationList: state => state.validCommunicationList,
    // filter: state => state.filter,
  },
};

export default bankStoreModule;