FROM node:lts-alpine

# install simple http server for serving static content

# make the 'app' folder the current working directory
WORKDIR /app2

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./
COPY . .
# install project dependencies
# RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)


EXPOSE 9790
CMD ["npm", "run", "start"]